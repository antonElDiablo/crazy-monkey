$(document).ready(function(){
  var audio_1 = document.getElementById('audio-1');
  var audio_2 = document.getElementById('audio-2');
  var btnPlay = $('.js-btn');
  var count = +btnPlay.attr('data-count');
  var numberCount = $('.js-count');
  var light = $('.js-light');
  var form = $('.js-form');
  var modal = $('.js-modal');
  var magic = throttle(startGame, 4000);
  var comb = $('.combinations__i');
  var form = $('.js-form');
  var finalSlot = $('.combinations__final');
  var thanks = $('.js-thanks');
  var clonnedThanks = thanks.clone();


  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }


  var baseTime = 1000;
  $('.overlay').on('click', function(){
    modal.removeClass('modal--show');
  });

  audio_1.volume = 0.5;
  audio_2.volume = 0.5;
  var mailfomr = $('.js-form-email');
  var mailError = mailfomr.find('.error');
  var formInput = mailfomr.find('.form__input');
  var btn = mailfomr.find('.form__button');

  var modalfomr = $('.js-modal');
  var modalError = modalfomr.find('.error');
  var modalInput = modalfomr.find('.form__input');
  var modalBtn = modalfomr.find('.form__button');

  modalBtn.on('click', function(ev) {
    ev.preventDefault();
    var isValid = validateEmail(modalInput);

    if (isValid) {
      clearForm(modalError, modalInput);
      $.ajax({
        url: 'http://www.smsintegrator.ru/api/v1/order/save',
        method: 'POST',
        crossDomain: true,
        data: {
          email: modalInput.val(),
          sub_id: getParameterByName('subid')
        },
        success: function(){
          modalfomr.find('.form--thanks').addClass('form--show');

        }
      })
    } else {
      hightLightError(modalError, modalInput);
    }
  });
  btn.on('click', function(ev){
    ev.preventDefault();
    var isValid = validateEmail(formInput);

    if (isValid) {
      clearForm(mailError, formInput);
      $.ajax({
        url: 'http://www.smsintegrator.ru/api/v1/order/save',
        method: 'POST',
        crossDomain: true,
        data: {
          email: formInput.val(),
          sub_id: getParameterByName('subid')
        },
        success: function(){
          mailfomr.removeClass('form--show');
          thanks.addClass('form--show');
        }
      })
    } else {
      hightLightError(mailError, formInput);
    }
  });

  function validateEmail(elem) {
    return /^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/.test(elem.val());
  }

  function hightLightError(elem, inp) {
    elem.addClass('error--show');
    inp.css('border-color', 'red')
  }

  function clearForm(elem, inp) {
    elem.removeClass('error--show');
    inp.css('border-color', 'black')
  }

  function throttle(func, ms) {

    var isThrottled = false,
        savedArgs,
        savedThis;

    function wrapper() {

      if (isThrottled) {
        savedArgs = arguments;
        savedThis = this;
        return;
      }

      func.apply(this, arguments);

      isThrottled = true;

      setTimeout(function() {
        isThrottled = false;
        if (savedArgs) {
          wrapper.apply(savedThis, savedArgs);
          savedArgs = savedThis = null;
        }
      }, ms);
    }

    return wrapper;
  }
  function setCurrentTry(index) {
    numberCount.removeClass('counter__img--active');
    numberCount.eq(index + 1).addClass('counter__img--active');
    light.eq(index).addClass('lights-list__item--unactive');
    comb.addClass('combinations__i--start-' + index);
    baseTime = 1000;
    if (index == 3) {
      setTimeout(function(){
        modal.addClass('modal--show');
      }, 3500)
    }
    form.removeClass('form--show');
    if (thanks.hasClass('form--show')) {
      thanks.removeClass('form--show');
    }
    setTimeout(function(){
      form.eq(index).addClass('form--show');
    },3500)
  }
  function startGame(index) {
    if (index == 3) {
      audio_2.play();
    } else {
      audio_1.play();
    }
    count++;
    btnPlay.attr('data-count', count);
    setCurrentTry(index);

  }

  btnPlay.on('click', function(ev){
    ev.preventDefault();
    if(count >= 0 && count < 4) {
      magic(count);
    }
  })
});
